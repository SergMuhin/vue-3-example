import { vi } from "vitest";

export function useExchangeRateStore() {
	return {
    exchangeRate: 2,
    prevExchangeRate: 1,
    updateExchangeRate: vi.fn()
}
}