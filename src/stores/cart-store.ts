import { computed, ref } from 'vue'
import { defineStore } from 'pinia'

import { useProductsStore } from '@/stores/products-store'

export const useCartStore = defineStore('cart', () => {
  const cartItems = ref([])

  const productsStore = useProductsStore()

  const total = computed(() =>
    cartItems.value.reduce((acc, curr) => acc + curr.price * curr.quantity, 0)
  )

  function addToCart({ product, quantity = 1 }) {
    const index = cartItems.value.findIndex((cartItem) => cartItem.id === product.id)
    if (index > -1) {
      cartItems.value[index].quantity += quantity
    } else {
      cartItems.value.push({
        id: product.id,
        price: product.price.current,
        title: product.title,
        quantity
      })
    }
  }

  function removeFromCart({ product, quantity = 1 }) {
    const index = cartItems.value.findIndex((item) => item.id === product.id)
    if (quantity < 0) {
      cartItems.value = cartItems.value.filter((item) => item.id !== product.id)
    } else {
      cartItems.value[index].quantity -= quantity
    }
  }

  function changeQuantity({ product, quantity }) {
    const index = cartItems.value.findIndex((item) => item.id === product.id)
    if (index > -1) {
      cartItems.value[index].quantity = quantity
    }
  }

  function updateCartPrices() {
    cartItems.value.forEach((item) => {
      const index = productsStore.products.findIndex((product) => item.id === product.id)
      if (index > -1) {
        item.price = productsStore.products[index].price.current
      }
    })
  }

  function getProduct(id) {
    const index = cartItems.value.findIndex((item) => item.id === id)
    if (index > -1) {
      return cartItems.value[index]
    }
  }

  function clear() {
    cartItems.value = []
  }

  return {
    cartItems,
    total,
    addToCart,
    removeFromCart,
    updateCartPrices,
    changeQuantity,
    getProduct,
    clear
  }
})
