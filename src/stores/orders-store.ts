import { ref } from 'vue'
import { defineStore } from 'pinia'

import { useProductsStore } from '@/stores/products-store'

export const useOrdersStore = defineStore('orders', () => {
  const orders = ref([])
  const productsStore = useProductsStore()

  function sendOrder({ items, total }) {
    productsStore.decreaseCount(items)

    orders.value.push({
      id: Date.now(),
      items,
      total
    })
  }

  return { orders, sendOrder }
})
