import { ref } from 'vue'
import { defineStore } from 'pinia'

import { FAKE_MAX_PRICE, FAKE_MIN_PRICE, IProduct, getProducts } from '@/services/product-service'
import { ICategory, getCategories } from '@/services/category-service'
import { ICartItem } from '@/services/cart-service'
import { groupBy } from '@/utils/array/group-by'
import getRandomNumberInRange from '@/utils/number/get-random-number-in-range'

import { useExchangeRateStore } from '@/stores/exchange-store'
import { useCartStore } from '@/stores/cart-store'

export const useProductsStore = defineStore('products', () => {
  const products = ref<IProduct[]>([])
  const categories = ref<ICategory[]>([])
  const loading = ref(true)
  const error = ref(null)

  const exchangeRateStore = useExchangeRateStore()
  const cartStore = useCartStore()

  async function getProductsData() {
    loading.value = true
    error.value = null

    try {
      const [productsResponse, categoriesResponse] = await Promise.all([
        getProducts(),
        getCategories()
      ])

      const { data: categoriesObj } = categoriesResponse

      products.value = productsResponse.data.Value.Goods.map((item) => {
        const { T: id, G: categoryId, P: count } = item

        return {
          id,
          category: {
            id: categoryId,
            title: categoriesObj[categoryId].G
          },
          title: categoriesObj[categoryId].B[id].N,
          price: {
            prev: null,
            current:
              getRandomNumberInRange(FAKE_MIN_PRICE, FAKE_MAX_PRICE) *
              exchangeRateStore.exchangeRate
          },
          count
        }
      })

      const groupedProducts = groupBy(products.value, (item) => item.category.id)

      for (const key in categoriesObj) {
        const items = groupedProducts[key] || []
        if (items.length) {
          categories.value.push({
            id: +key,
            title: categoriesObj[key].G,
            items
          })
        }
      }
    } catch (err) {
      error.value = err
    } finally {
      loading.value = false
    }
  }

  function updateProductPrices() {
    products.value.forEach((item) => {
      item.price.prev = item.price.current
      item.price.current =
        getRandomNumberInRange(FAKE_MIN_PRICE, FAKE_MAX_PRICE) * exchangeRateStore.exchangeRate
    })
    cartStore.updateCartPrices()
  }

  function getProduct(id: number) {
    const index = products.value.findIndex((item) => item.id === id)
    if (index > -1) {
      return products.value[index]
    }
  }

  function decreaseCount(items: ICartItem[]) {
    items.forEach((item) => {
      const index = products.value.findIndex((product) => product.id === item.id)
      if (index > -1) {
        products.value[index].count = products.value[index].count - item.quantity
      }
    })
  }

  return {
    products,
    categories,
    loading,
    error,
    getProductsData,
    updateProductPrices,
    getProduct,
    decreaseCount
  }
})
