import { ref } from 'vue'
import { defineStore } from 'pinia'

import getRandomNumberInRange from '@/utils/number/get-random-number-in-range'
import { MIN_EXCHANGE_RATE, MAX_EXCHANGE_RATE } from '@/services/exchange-service'

export const useExchangeRateStore = defineStore('exchangeRate', () => {
  const exchangeRate = ref(getRandomNumberInRange(MIN_EXCHANGE_RATE, MAX_EXCHANGE_RATE))
  const prevExchangeRate = ref(0)

  function updateExchangeRate() {
    prevExchangeRate.value = exchangeRate.value
    exchangeRate.value = getRandomNumberInRange(MIN_EXCHANGE_RATE, MAX_EXCHANGE_RATE)
  }

  return { exchangeRate, prevExchangeRate, updateExchangeRate }
})
