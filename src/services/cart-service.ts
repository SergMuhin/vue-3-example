export const LIMITED_STOCK_COUNT = 5

export interface ICartItem {
  id: number
  price: number
  title: string
  quantity: number
}
