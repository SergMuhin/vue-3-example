import { ICartItem } from './cart-service'

export interface Iorder {}

export interface IOrder {
  id: number
  total: number
  items: ICartItem[]
}
