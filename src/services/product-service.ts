import axios from '@/config/axios'

export interface IProduct {
  id: number
  title: string
  count: number
  category: {
    id: number
    title: string
  }
  price: {
    current: number
    prev: number
  }
}

export const FAKE_MIN_PRICE = 15
export const FAKE_MAX_PRICE = 70

export function getProducts() {
  return axios.get('data.json')
}
