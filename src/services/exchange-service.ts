export const MIN_EXCHANGE_RATE = 80
export const MAX_EXCHANGE_RATE = 100

export const MIN_UPDATE_PERIOD = 2000
export const MAX_UPDATE_PERIOD = 10_000
