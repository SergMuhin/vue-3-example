import axios from '@/config/axios'
import { IProduct } from './product-service'

export interface ICategory {
  id: number
  title: string
  items: IProduct[]
}

export function getCategories() {
  return axios.get('names.json')
}
