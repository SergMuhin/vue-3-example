import Home from '@/pages/home-page.vue'
import Cart from '@/pages/cart-page.vue'
import Orders from '@/pages/orders-page.vue'
import NotFound from '@/pages/not-found-page.vue'

import { RouteName } from '@/router/routes/names'

export default [
  { path: '/', name: RouteName.Home, component: Home },
  { path: '/cart', name: RouteName.Cart, component: Cart },
  { path: '/orders', name: RouteName.Orders, component: Orders },
  { path: '/:pathMatch(.*)*', component: NotFound }
]
