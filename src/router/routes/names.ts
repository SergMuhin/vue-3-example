export enum RouteName {
  Home = 'home',
  Cart = 'cart',
  Orders = 'orders'
}
