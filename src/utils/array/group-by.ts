export function groupBy(array, fn) {
  const map = {}

  for (let i = 0; i < array.length; i++) {
    const item = array[i]
    const key = fn(item)
    if (key in map) {
      map[key].push(item)
    } else {
      map[key] = [item]
    }
  }

  return map
}
