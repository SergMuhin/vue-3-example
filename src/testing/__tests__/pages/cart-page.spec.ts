import { describe, it, expect } from 'vitest'

import CartPage from '@/pages/cart-page.vue'
import Cart from '@/components/cart/cart.vue'
import SectionTitle from '@/components/base/section-title.vue'
import { shallowMount } from '@vue/test-utils'

describe('CartPage', () => {
  it('render correctly', async () => {
    const wrapper = shallowMount(CartPage)

    expect(wrapper.getComponent(SectionTitle)).toBeTruthy()
    expect(wrapper.getComponent(Cart)).toBeTruthy()
  })

  it('hide title when cart emits order-sent', async () => {
    const wrapper = shallowMount(CartPage, {
      global: {
        stubs: {
          Cart: {
            template: '<span></span>',
            $emit: { 'order-sent': () => {} }
          }
        }
      }
    })

    await wrapper.getComponent(Cart).vm.$emit('order-sent')
    // @ts-ignore
    expect(wrapper.vm.showTitle).toBe(false)
  })
})
