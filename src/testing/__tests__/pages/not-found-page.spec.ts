import { describe, it, expect } from 'vitest'

import NotFound from '@/pages/not-found-page.vue'
import SectionTitle from '@/components/base/section-title.vue'
import { RouterLinkStub, mount } from '@vue/test-utils'
import { RouteName } from '@/router/routes/names'

describe('NotFoundPage', () => {
  it('render correctly', () => {
    const wrapper = mount(NotFound, {
      global: {
        stubs: {
          RouterLink: RouterLinkStub
        }
      }
    })

    const title = wrapper.findComponent(SectionTitle)
    expect(title.element.tagName).toEqual('H2')
    expect(title.text()).toEqual('Page you looking for is not found 😔')

    const homeLink = wrapper.findComponent(RouterLinkStub)
    expect(homeLink.text()).toEqual('Go to products')
    expect(homeLink.props('to')).toStrictEqual({ name: RouteName.Home })
  })
})
