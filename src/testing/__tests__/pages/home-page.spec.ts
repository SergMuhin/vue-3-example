import { describe, it, expect } from 'vitest'

import HomePage from '@/pages/home-page.vue'
import ProductsList from '@/components/products/products-list.vue'
import SectionTitle from '@/components/base/section-title.vue'
import { shallowMount } from '@vue/test-utils'

describe('HomePage', () => {
  it('render correctly', () => {
    const wrapper = shallowMount(HomePage)

    const title = wrapper.getComponent(SectionTitle)
    expect(title.element.tagName).toBe('SECTION-TITLE-STUB')

    expect(wrapper.getComponent(ProductsList).isVisible()).toBe(true)
  })
})
