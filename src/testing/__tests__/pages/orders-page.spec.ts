import { describe, it, expect } from 'vitest'

import Orders from '@/pages/orders-page.vue'
import OrdersList from '@/components/orders/orders-list.vue'
import SectionTitle from '@/components/base/section-title.vue'
import { shallowMount } from '@vue/test-utils'

describe('OrdersPage', () => {
  it('render correctly', () => {
    const wrapper = shallowMount(Orders)

    const title = wrapper.getComponent(SectionTitle)
    expect(title.element.tagName).toBe('SECTION-TITLE-STUB')

    expect(wrapper.getComponent(OrdersList).isVisible()).toBe(true)
  })
})
