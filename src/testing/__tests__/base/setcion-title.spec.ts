import { describe, it, expect } from 'vitest'

import { shallowMount } from '@vue/test-utils'
import SectionTitle from '@/components/base/section-title.vue'

describe('SectionTitle', () => {
  it('render correctly', () => {
    const wrapper = shallowMount(SectionTitle, {
      slots: {
        default: 'Default'
      }
    })

    const title = wrapper.get('[data-testid="section-title"]')
    expect(title).toBeTruthy()
    expect(title.text()).toEqual('Default')
  })
})
