import { describe, it, expect } from 'vitest'

import { mount, shallowMount } from '@vue/test-utils'
import CounterInput from '@/components/base/counter-input.vue'

const couinterInputOpts = {
  props: {
    amount: 5,
    max: 10
  }
}

describe('CounterInput', () => {
  it('render correctly', () => {
    const wrapper = mount(CounterInput, couinterInputOpts)

    expect(wrapper.props('amount')).toEqual(5)
    expect(wrapper.props('max')).toEqual(10)

    expect(wrapper.get('.counter-input')).toBeTruthy()
    expect(wrapper.get('[data-testid="counter-input-decrement-btn"]')).toBeTruthy()
    expect(wrapper.get('[data-testid="counter-input-increment-btn"]')).toBeTruthy()
    expect(wrapper.get('.counter-input__field')).toBeTruthy()
  })

  it('decrement correctly', async () => {
    const wrapper = shallowMount(CounterInput, couinterInputOpts)

    const button = wrapper.get('[data-testid="counter-input-decrement-btn"]')

    expect(button.text()).toEqual('➖')

    await button.trigger('click')

    expect(wrapper.emitted()).toHaveProperty('update:amount')
    expect(wrapper.emitted()['update:amount'][0][0]).toEqual(couinterInputOpts.props.amount - 1)
  })

  it('not decrement when amount === 1', async () => {
    const wrapper = shallowMount(CounterInput, {
      props: {
        amount: 1,
        max: 10
      }
    })

    const button = wrapper.get('[data-testid="counter-input-decrement-btn"]')

    await button.trigger('click')

    expect(wrapper.emitted()).not.toHaveProperty('update:amount')
  })

  it('increment correctly', async () => {
    const wrapper = shallowMount(CounterInput, couinterInputOpts)

    const button = wrapper.get('[data-testid="counter-input-increment-btn"]')

    expect(button.text()).toEqual('➕')

    await button.trigger('click')

    expect(wrapper.emitted()).toHaveProperty('update:amount')
    expect(wrapper.emitted()['update:amount'][0][0]).toEqual(couinterInputOpts.props.amount + 1)
  })

  it('input changes correctly', async () => {
    const wrapper = shallowMount(CounterInput, couinterInputOpts)

    const input = wrapper.get('.counter-input__field')

    // @ts-ignore
    expect(input.element.value).toEqual('5')
    // @ts-ignore
    expect(input.element.max).toEqual('10')
    // @ts-ignore
    expect(input.element.type).toEqual('number')

    await input.setValue(7)
    expect(wrapper.emitted()['update:amount'][0][0]).toEqual(7)

    await input.setValue(13)
    expect(wrapper.emitted()['update:amount'][1][0]).toEqual(couinterInputOpts.props.max)
  })
})
