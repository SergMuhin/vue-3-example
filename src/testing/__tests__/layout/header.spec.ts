import { describe, it, expect } from 'vitest'

import { RouterLinkStub, shallowMount } from '@vue/test-utils'
import TheHeader from '@/components/layout/the-header.vue'
import { RouteName } from '@/router/routes/names'

describe('Header', () => {
  it('render correctly', () => {
    const wrapper = shallowMount(TheHeader)

    const header = wrapper.get('.header')
    expect(header.isVisible()).toBe(true)

    const headerText = wrapper.get('.header__text')
    expect(headerText.text()).toEqual('vue-3-test-example')
  })

  it('contains proper header links', () => {
    const wrapper = shallowMount(TheHeader, {
      global: {
        stubs: {
          RouterLink: RouterLinkStub
        }
      }
    })

    const headerLinks = wrapper.findAllComponents(RouterLinkStub)

    expect(headerLinks.length).toEqual(3)
    expect(headerLinks[0].props('to')).toStrictEqual({ name: RouteName.Home })
    expect(headerLinks[1].props('to')).toStrictEqual({ name: RouteName.Cart })
    expect(headerLinks[2].props('to')).toStrictEqual({ name: RouteName.Orders })
  })
})
