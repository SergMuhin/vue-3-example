import { describe, it, expect } from 'vitest'

import { shallowMount } from '@vue/test-utils'
import TheFooter from '@/components/layout/the-footer.vue'

describe('Footer', () => {
  it('render correctly', () => {
    const wrapper = shallowMount(TheFooter)

    expect(wrapper.get('[data-testid="made"]')).toBeTruthy()

    expect(wrapper.get('[data-testid="made"]').text()).toEqual('Made with 🖤 by Serg Mukhin')

    expect(wrapper.get('[data-testid="year"]').text()).toEqual('2️⃣0️⃣2️⃣3️⃣')
  })
})
